<?php

require_once(__dir__."/page.php");
require_once(__dir__."/error.php");
require_once(__dir__."/settings.php");
require_once(__dir__."/session.php");

/**
 * \brief Dispatches the path to the right page object
 *
 * When given the path, it finds the closest matching php file, and executes it.
 * The file must define a variable called \c $page which is a Page instance.
 *
 * It matches the path by scanning each path component and trying to find a
 * php file with the same name (and .php extension) or a file called index.php
 * in the directory with the same name as the path prefix.
 * The file found deepest in the directory tree is the one the path is passed to,
 * removing the matching prefix when calling dispatch to the selected page.
 *
 * In order to use this, instantiate an object in a php file that receives all
 * pages, and pass \c $_SERVER["REQUEST_URI"] (or the relevant string).
 *
 * Example Apache config
 * \code{.htaccess}
 * RewriteBase /
 *
 * # If you want to add trailing slashes
 * RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} !-f
 * RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} !-d [OR]
 * RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI}/index.php -f
 * RewriteRule (.*[^/])$ $1/ [L,R]
 *
 * RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} !-f
 * RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} !-d [OR]
 * RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI}/index.php -f
 * RewriteRule .* site.php
 * \endcode
 * \code
 * <?php
 * require_once(__dir__."/private/lib/site.php");
 * $site = new Site(__dir__);
 * $site->dispatch($_SERVER["REQUEST_URI"]);
 * \endcode
 */
class Site extends PageController
{
    protected $errors_500 = E_ERROR|E_PARSE|E_CORE_ERROR|E_COMPILE_ERROR|E_USER_ERROR;
    public $debug = false;
    public $php_errors = array();
    protected $default_settings = array(
        "debug" => false,
        "metadata" => []
    );

    function __construct($base_path, $settings_file)
    {
        $this->base_path = $base_path;
        $this->error_shown = false;
        $this->error_pages = array();
        $this->settings = new Settings($settings_file, $this->default_settings);
        $this->session = new Session();
    }

    /**
     * \note It can convert the http code (eg: to hide forbidden pages)
     * \returns The page to handle the code
     * The return value can be:
     * * \b null - there is no handler for the code
     * * string  - used to load the page the same way it would load from a url
     * * Page    - an already created page object
     */
    function get_error_page(&$code)
    {
        return null;
    }

    function get_error_page_object(&$code)
    {
        $page_or_file = $this->get_error_page($code);
        if ( !$page_or_file )
            return null;
        if ( $page_or_file instanceof Page )
            return $page_or_file;
        if ( isset($this->error_pages[$page_or_file]) )
            return $this->error_pages[$page_or_file];
        include_once($page_or_file);
        if ( !isset($page) )
            trigger_error("Page for $code in $page_or_file not found", E_USER_WARNING);
        $this->error_pages[$page_or_file] = $page;
        return $page;
    }

    function dispatch_error($code, $path)
    {
        if ( $this->error_shown )
            return false;

        // Displaying an error page, discard existing output (if any)
        ob_end_clean();

        $visited = array();
        $page = $this->get_error_page_object($code);

        http_response_code($code);
        if ( !$page )
            return false;

        $page->dispatch($path);
        $this->error_shown = true;
        return true;
    }

    function show_error_page()
    {
        $this->dispatch_error(500, $_SERVER["REQUEST_URI"]);
    }

    function add_error($error)
    {
        if ( ! in_array($error, $this->php_errors) )
            $this->php_errors []= $error;
    }

    function register_error_handler()
    {
        $that = $this;

        $error_handler = function($error) use ($that)
        {
            if ( !$error )
                return false;

            $that->add_error($error);

            if ( $error->should_report() )
                $error->log();

            if ( $that->errors_500 & $error->type )
            {
                $that->show_error_page();
                return true;
            }
            return false;
        };

        set_error_handler(function($errno, $errstr, $errfile, $errline) use ($error_handler)
        {
            $error_handler(new PhpError($errno, $errstr, $errfile, $errline));
        });

        # For some reason set_error_handler doesn't handle all errors
        register_shutdown_function(function() use ($error_handler)
        {
            if ( $error_handler(PhpError::get_last()) )
            {
                return;
            }

            // Print all the output
            ob_end_flush();
        });
    }

    function dispatch($path)
    {
        // Capture output so to emit valid HTML on PHP errors.
        ob_start();

        $code = http_response_code();

        if ( $code == 200 && strpos($path, "//") !== false )
        {
            $code = 301;
            redirect(preg_replace("(//+)", "/", $path), $code);
        }

        if ( $code != 200 )
        {
            if ( $this->dispatch_error($code, $path) )
                return true;
        }

        try {
            return parent::dispatch($path);
        } catch(HttpStatus $e) {
            return $this->dispatch_error($e->code, $path);
        }
    }

    function dispatch_to_self($path)
    {
        return $this->dispatch_to_page("{$this->base_path}/index.php", $path);
    }

    function base_path()
    {
        return $this->base_path;
    }

    function dispatch_to_other($path)
    {
        $base_path = $this->base_path();

        $match = null;
        $match_prefix = array();
        $current_prefix = array();

        $attempt = function ($file) use (&$match, &$match_prefix, &$current_prefix)
        {
            if ( file_exists($file) && $match != $file )
            {
                $match = $file;
                $match_prefix = $current_prefix;
                return true;
            }
            return false;
        };

        $attempt(path_join($base_path, "index.php"));

        foreach ( explode("/", $path) as $path_comp )
        {
            array_push($current_prefix, $path_comp);
            $basename = path_join($base_path, implode("/", $current_prefix));
            $attempt("$basename.php");
            $attempt(path_join($basename, "index.php"));
        }

        if ( $match )
        {
            $prefix = implode("/", $match_prefix);
            $suffix = substr($path, strlen($prefix));
            return $this->dispatch_to_page($match, $suffix);
        }

        return false;
    }

    protected function dispatch_to_page($page_file, $path)
    {
        if ( realpath($page_file) == $this->self_filename() )
        {
            return false;
        }

        include($page_file);
        if ( isset($page) )
            return $page->dispatch($path);
        return false;
    }

    protected function unhandled_page($path)
    {
        return $this->dispatch_error(404, $path);
    }
}
