<?php

/**
 * \brief Makes the current response into a redirect
 */
function redirect($url, $status=302)
{
    http_response_code($status);
    if ( $url != null )
        header("Location: $url");
}


/**
 * \brief Exception raised to result in a response with the given status
 */
class HttpStatus extends Exception
{
    public $code;
    function __construct($code)
    {
        $this->code = $code;
    }
}
