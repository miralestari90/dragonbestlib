<?php

require_once(__dir__."/form.php");
require_once(__dir__."/../db/model.php");
require_once(__dir__."/../db/relations.php");


class SelectModelWidget extends AbstractSelectWidget
{

    function __construct(Form $form, $name, Query $query, $empty_choice = null, $initial = null, $label = null, $required = true, $read_only = false)
    {
        parent::__construct($form, $name, $empty_choice, $initial, $label, $required, $read_only);
        $this->query = $query;
        $this->result = $query->select();
    }

    function value_from_input($value)
    {
        $obj = $this->query->where($this->query->model::$primary_key, "=", $value).first();
        if ( !$obj )
            throw new ValidationError("invalid choice");
    }

    function choices()
    {
        return $this->result;
    }

    protected function compare_choice_value($choice, $value)
    {
        return $choice->pk() == $value;
    }

    protected function choice_to_element($choice, $is_current)
    {
        return $this->option($choice->pk(), (string)$choice, $is_current);
    }
}

class RegistryInputHelper
{
    function __construct($class)
    {
        $this->class = $class;
        $this->constructor = [new ReflectionClass($class), "newInstance"];
    }

    function __invoke(BoundField $field, Form $form, $name, $label)
    {
        return static::create_widget($this->constructor, $field, $form, $name, $label);
    }

    static function create_widget($widget_callback, BoundField $field, Form $form, $name, $label)
    {
        if ( $label === null )
        {
            $label = slug_to_title($field->field->field_name);
        }
        if ( $name === null )
        {
            $name = $field->field->field_name;
        }
        return $widget_callback($form, $name, $field->get_value(), $label, $field->field->default === null and !$field->field->null);
    }
}

final class ModelFieldWidgetRegistry
{
    private static $instance;
    private $registry_types = [];
    private $registry_classes = [];

    private function __construct()
    {
        $this->register_builtin("CharField", new RegistryInputHelper("InputWidget"));
        $this->register_builtin("IntegerField", new RegistryInputHelper("NumberInputWidget"));
        $this->register_builtin("BooleanField", new RegistryInputHelper("CheckboxWidget"));
        $this->register_builtin("DateField", new RegistryInputHelper("DateInputWidget"));
        $this->register_builtin("DateTimeField", new RegistryInputHelper("DateTimeInputWidget"));
        $this->register_builtin("DecimalField", new RegistryInputHelper("NumberInputWidget"));
        $this->register_builtin("ForeignKeyField", function (BoundField $field, Form $form, $name, $label){
            return new SelectModelWidget(
                $form,
                $field->field->target_model::query(),
                "-----",
                $field->get_value(),
                $label,
                $field->default === null and !$field->null
            );
        });
    }

    static function instance() : ModelFieldWidgetRegistry
    {
        if ( !isset(self::$instance) )
        {
            $class = self::class;
            self::$instance = new $class();
        }

        return self::$instance;
    }

    function register_column_type($column_type, $functor)
    {
        $this->registry_types[$column_type] = $functor;
    }

    function register($class, $functor)
    {
        array_unshift($this->registry_classes, [$class, $functor]);
    }

    function register_builtin($class, $functor)
    {
        $this->register($class, $functor);
        $this->register_column_type($class::$name, $functor);
    }

    private function best_match(Field $field)
    {
        foreach ( $this->registry_classes as $clr )
        {
            if ( $field instanceof $clr[0] )
                return $clr[1];
        }

        if ( isset($this->registry_types[$field->column_type()]) )
            return $this->registry_types[$field->column_type()];

        throw new Exception("No widget registered for this model field");
    }

    function widget(BoundField $field, Form $form, $name = null, $label = null)
    {
        $functor = $this->best_match($field->field);
        return $functor($field, $form, $name, $label);
    }

    function custom_widget($widget, BoundField $field, Form $form, $name = null, $label = null)
    {
        if ( is_object($widget) and $widget instanceof FormWidget )
            return $widget;
        if ( is_callable($widget) )
            $constructor = $widget;
        else
            $constructor = [new ReflectionClass($widget), "newInstance"];
        return RegistryInputHelper::create_widget($constructor, $field, $form, $name, $label);
    }
}

class ModelForm extends Form
{
    function __construct($model, $fields, $custom_widgets = [], $input_array = [], $initial = [], $prefix = "")
    {
        if ( $model instanceof Model )
            $this->instance = $model;
        else
            $this->instance = new $model();

        $this->fields = [];

        foreach ( $fields as $field_name )
        {
            $field = $this->instance->get_field($field_name);
            $this->fields []= $field;
            $initial[$field->field->field_name] = $field->get_value();
        }
        $this->custom_widgets = $custom_widgets;

        parent::__construct($input_array, $initial, $prefix);
    }

    function add_field($field_name)
    {
        $field = $this->instance->get_field($field_name);
        $this->fields []= $field;
        $this->initial[$field->field->field_name] = $field->get_value();
        $this->widgets []= $this->widget_from_field($field);
    }

    function load_widgets()
    {
        $widgets = [];
        foreach ( $this->fields as $field )
        {
            $widgets []= $this->widget_from_field($field);
        }
        return $widgets;
    }

    function widget_from_field(BoundField $field, $label = null)
    {
        $reg = ModelFieldWidgetRegistry::instance();
        if ( isset($this->custom_widgets[$field->field->field_name]) )
        {
            $widget = $this->custom_widgets[$field->field->field_name];
            return $reg->custom_widget($widget, $field, $this, null, $label);
        }

        return $reg->widget($field, $this, null, $label);
    }

    function save($commit = true)
    {
        foreach ( $this->data() as $field_name => $value )
        {
            $this->instance->get_field($field_name)->set_value($value);
        }
        if ( $commit )
            $this->instance->save();
        return $this->instance;
    }
}
