<?php

require_once(__dir__."/widgets.php");


class Form extends PageWidget
{
    public $initial = [];
    public $widgets = [];
    private $errors = null;
    public $data = null;

    function __construct($input_array = [], $initial = [], $prefix = "")
    {
        $this->raw_data = $input_array;
        $this->prefix = $prefix;
        $this->initial = $initial;
        $widgets = $this->load_widgets();
        foreach ( $widgets as $field )
        {
            if ( isset($this->initial[$field->name]) )
                $field->initial = $this->initial[$field->name];
            $this->widgets[$field->name] = $field;
        }
    }

    /**
     *  \brief Returns an array of FieldWidget
     */
    function load_widgets()
    {
        return [];
    }

    function set_initial_data($initial)
    {
        $this->initial = $initial;
        foreach ( $initial as $name => $value )
        {
            $this->widgets[$name]->initial = $value;
        }
    }

    function is_valid()
    {
        if ( $this->errors === null )
            return $this->validate();
        return !(bool)$this->errors;
    }

    function add_error($for, $message)
    {
        if ( $this->errors === null )
            $this->errors = [$for => [$message]];
        else if ( !isset($this->errors[$for]) )
            $this->errors[$for] = [$message];
        else
            $this->errors[$for][] = $message;
    }

    function get_errors($for=null)
    {
        if ( $for === null )
            return $this->errors;
        return $this->errors[$for] ?? [];
    }

    function validate()
    {
        $this->errors = [];
        $this->data = [];
        foreach ( $this->widgets as $widget )
        {
            try
            {
                $this->data[$widget->name] = $widget->get_value($this->raw_data);
            }
            catch ( ValidationError $error )
            {
                $this->add_error($widget->name, $error->getMessage());
            }
        }

        try
        {
            $this->post_validate();
        }
        catch ( ValidationError $error )
        {
            $this->add_error(null, $error->getMessage());
        }

        return !(bool)$this->errors;
    }

    function data()
    {
        if ( $this->data === null )
            $this->validate();
        return $this->data;
    }

    /**
     * \brief hook for custom validation
     */
    protected function post_validate()
    {
    }

    function widgets()
    {
        return array_values($this->widgets());
    }

    function element($container_type=null)
    {
        $list = new DisplayElementList();
        foreach ( $this->widgets as $widget )
            $list->items []= $this->widget_to_element($widget, $container_type);
        return $list;
    }

    protected function widget_to_element(FormWidget $widget, $container_type)
    {
        if ( !$widget->visible() )
            return $widget->input();
        if ( $container_type === null )
            return new DisplayElementList([$widget->label(), $widget->input()]);
        else if ( $container_type === "tr" )
            return mkelement(["tr", [], [
                ["td", [], $widget->label()],
                ["td", [], $widget->input()],
            ]]);
        else
            return mkelement([$container_type, [], [$widget->label(), $widget->input()]]);
    }
}
