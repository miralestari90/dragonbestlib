<?php

require_once(__dir__."/../html.php");


class ValidationError extends Exception
{
}

abstract class FormWidget extends PageWidget
{
    static $visible = true;

    function __construct(Form $form, $name, $initial = null, $label = null, $required = true, $read_only = false)
    {
        $this->form = $form;
        $this->name = $name;
        $this->label = $label === null ? slug_to_title($name) : $label;
        $this->required = $required;
        $this->initial = $initial;
        $this->read_only = $read_only;
    }

    /**
     * \brief Element for the input label
     */
    function label($attrs=[])
    {
        return mkelement(["label", array_merge(["for"=>$this->id()], $attrs), $this->label]);
    }

    /**
     * \brief DOM Element ID
     */
    function id()
    {
        return "{$this->form->prefix}{$this->name}";
    }

    /**
     * \brief Common attributes for the input element
     */
    function input_attributes()
    {
        $attrs = [
            "id" => $this->id(),
            "name" => $this->name,
        ];

        if ( $this->required )
            $attrs["required"] = "required";

        if ( $this->read_only )
            $attrs["readonly"] = "readonly";

        return $attrs;
    }

    /**
     * \brief Input element
     */
    abstract function input($attrs=[]);

    /**
     * \brief Return a validated PHP object from the array of strings
     */
    function get_value($input_array)
    {
        if ( $this->read_only )
            return $this->initial;

        if ( !isset($input_array[$this->name]) )
        {
            if ( $this->required )
                throw new ValidationError("this field is required");
            return $this->initial;
        }
        return $this->value_from_input($input_array[$this->name]);
    }

    /**
     * \brief Returns the current value for this field in the parent form
     */
    function get_current_value()
    {
        if ( $this->form->data )
            return $this->form->data[$this->name];
        return $this->initial;
    }

    /**
     * \brief Return a correct PHP value or throw ValidationError
     */
    protected function value_from_input($value)
    {
        return $value;
    }

    /**
     * \brief Serializes a PHP object to be used as input value
     */
    function serialize_php_value($value)
    {
        return (string)$value;
    }

    function element()
    {
        return $this->input();
    }

    abstract function type();

    function visible()
    {
        return static::$visible;
    }

}

class InputWidget extends FormWidget
{
    static $type = "string";

    function input($attrs=[])
    {
        $attrs = $this->input_attributes();
        $attrs["type"] = $this->type();
        $this->value_attrs($attrs);
        return mkelement(["input", $attrs]);
    }

    protected function value_attrs(&$attrs)
    {
        $attrs["value"] = $this->serialize_php_value($this->get_current_value());
    }

    function serialize_php_value($value)
    {
        return (string)$value;
    }

    function type()
    {
        return static::$type;
    }
}

class CheckboxWidget extends InputWidget
{
    static $type = "checkbox";

    protected function value_attrs(&$attrs)
    {
        if ( $this->get_current_value() )
            $attrs["checked"] = "checked";
    }

    function get_value($input_array)
    {
        if ( $this->read_only )
            return $this->initial;

        return isset($input_array[$this->name]) && $input_array[$this->name];
    }
}

class NumberInputWidget extends InputWidget
{
    static $type = "number";

    protected function value_from_input($value)
    {
        if ( !is_numeric($value) )
            throw new ValidationError("expected a number");
        return (float)$value;
    }
}

class HiddenInputWidget extends InputWidget
{
    static $type = "hidden";
    static $visible = false;
}

class DateTimeInputWidget extends InputWidget
{
    static $type = "string";
    static $format = "Y-m-d H:i:s";

    function format()
    {
        return static::$format;
    }

    function serialize_php_value($value)
    {
        if ( $value === null )
            return "";
        return $value->format($this->format());
    }

    function value_from_input($value)
    {
        $value = DateTime::createFromFormat($this->format(), $value);
        if ( !$value === false )
            throw new ValidationError("invalid date format");
        return $value;
    }
}

class DateInputWidget extends DateTimeInputWidget
{
    static $type = "date";
    static $format = "Y-m-d";
}

class TimeInputWidget extends DateTimeInputWidget
{
    static $type = "time";
    static $format = "H:i:s";
}

abstract class AbstractSelectWidget extends FormWidget
{
    function __construct(Form $form, $name, $empty_choice = null, $initial = null, $label = null, $required = true, $read_only = false)
    {
        parent::__construct($form, $name, $initial, $label, $required, $read_only);
        $this->empty_choice = $empty_choice;
    }

    /**
     * \brief Returns an array of choices as PHP objects
     */
    abstract function choices();

    /**
     * \brief Returns an option HTML element from an element of $this->choices()
     */
    abstract protected function choice_to_element($choice, $is_current);

    /**
     * \brief Compares an element of $this->choices() with a scalar
     */
    abstract protected function compare_choice_value($choice, $value);

    function input($attrs=[])
    {
        $attrs = $this->input_attributes();
        $this->value_attrs($attrs);
        $options = [];
        $current = $this->get_current_value();
        $found_current = false;
        foreach ( $this->choices() as $choice )
        {
            $is_current = !$found_current || $this->compare_choice_value($choice, $current);
            $options []= $this->choice_to_element($choice, $is_current);
        }
        if ( $this->empty_choice !== null )
            array_unshift($options, $this->option(null, $this->empty_choice, !$found_current, false));
        return mkelement(["select", $attrs, $options]);
    }

    protected function option($value, $display = null, $is_current = false, $enabled = true)
    {
        $attrs = ["value" => $value];
        if ( !$enabled )
            $attrs["disabled"] = "disabled";
        if ( $is_current )
            $attrs["selected"] = "selected";

        if ( $display === null )
            $display = $value;

        return mkelement(["option", $attrs, $display]);
    }


    function type()
    {
        return "select";
    }
}

class SelectWidget extends AbstractSelectWidget
{

    static function unassoc_choices($choices)
    {
        return array_combine($choices, $choices);
    }

    function __construct(Form $form, $name, $choices = [], $empty_choice = null, $initial = null, $label = null, $required = true, $read_only = false)
    {
        parent::__construct($form, $name, $empty_choice, $initial, $label, $required, $read_only);


        $this->choices = $choices;
        foreach ( $this->choices as $value => &$name )
        {
            $name = [$value, $name];
        }
    }

    function value_from_input($value)
    {
        if ( isset($this->choices[$value]) )
            return $this->choices[$value][0];
        throw new ValidationError("invalid choice");
    }

    function choices()
    {
        return $this->choices;
    }

    protected function compare_choice_value($choice, $value)
    {
        return $choice[0] == $value;
    }

    protected function choice_to_element($choice, $is_current)
    {
        return $this->option($choice[0], $choice[1], $is_current);
    }
}
