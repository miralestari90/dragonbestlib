<?php

class DbConfigurationError extends Exception
{
}

class DbValidationError extends Exception
{
}

class DbRuntimeError extends Exception
{
}
