<?php
$namespaceDefinition

use $useClassName;
//REPLACErequire_once(__dir__."/$includePath");

class $className extends $baseClassName
{
    /**
     * Change Models Method.
     *
     * Write your reversible migrations using this method.
     *
     * The following commands can be used in this method and the migration will
     * automatically reverse them when rolling back:
     *
     *    create_model
     *    add_field
     *    rename_field
     *    alter_field
     *    remove_field
     *    remove_model
     *
     */
    public function change_models()
    {
        //REPLACE$generatedMigration
    }
}