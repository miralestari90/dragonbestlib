<?php


require_once("errors.php");

abstract class Field
{
    static $types = [];

    function __construct($options)
    {
        $this->options = $options;
        $this->load_options();
    }

    protected function load_options()
    {
        $this->field_name = $this->get_option("field_name");
        $this->model_name = $this->get_option("model_name");
        $this->null = $this->get_option("null", false, false);
        $this->default = $this->get_option("default", false, null);

        if ( array_key_exists("column_name", $this->options) )
            $this->column_name = $this->options["column_name"];
        else
            $this->column_name = $this->default_column_name();
    }

    protected function get_option($name, $required = true, $default = null)
    {
        if ( array_key_exists($name, $this->options) )
            return $this->options[$name];

        if ( !$required )
        {
            $this->options[$name] = $default;
            return $default;
        }

        $type_name = static::$name;
        throw new DbConfigurationError("$name is required for field of type $type_name");
    }

    function php_to_db($value)
    {
        if ( $value === null )
        {
            if ( $this->null )
                return $value;
            throw new DbValidationError("Null {$this->column_name}");
        }
        return $this->on_php_to_db($value);
    }

    /**
     * \brief Like php_to_db but \p $value is never null
     */
    protected function on_php_to_db($value)
    {
        return $value;
    }

    function db_to_php($value)
    {
        if ( $value === null )
            return $value;
        return $this->on_db_to_php($value);
    }

    /**
     * \brief Like db_to_php but \p $value is never null
     */
    protected function on_db_to_php($value)
    {
        return $value;
    }

    function default_value()
    {
        if ( $this->default === null && !$this->null )
        {
            $type_name = static::$name;
            throw new DbRuntimeError("$type_name does not specify a default value");
        }
        return $this->default;
    }

    static function register()
    {
        self::$types[static::$name] = static::class;
    }

    function default_column_name()
    {
        return $this->options["field_name"];
    }

    static function get_field($model, $name, $options)
    {
        $type_name = $options[0];

        if ( !isset(self::$types[$type_name]) )
            throw new DbConfigurationError("$type_name is not a valid field type");

        $options["model_name"] = $model;
        $options["field_name"] = $name;
        $class = self::$types[$type_name];
        return new $class($options);
    }

    function column_type()
    {
        return static::$name;
    }

    function to_migration_options()
    {
        if ( !isset(static::$name) )
            return null;

        $options = $this->options;
        if ( isset($options["built_in"]) )
            return null;

        if ( !isset($options["has_column"]) )
            $options["has_column"] = true;
        unset($options["model_name"]);
        unset($options["field_name"]);
        $options["column_name"] = $this->column_name;
        $options["column_type"] = $this->column_type();
        ksort($options, SORT_NATURAL);
        return $options;
    }
}

/**
 * \brief Field associated with a model instance
 */
class BoundField
{
    function __construct(Field $field, &$data)
    {
        $this->field = $field;
        $this->data = &$data;
    }

    function has_value()
    {
        return array_key_exists($this->field->column_name, $this->data);
    }

    function get_value()
    {
        if ( !$this->has_value() )
            return $this->field->default_value();
        return $this->field->db_to_php($this->data[$this->field->column_name]);
    }

    function set_value($value)
    {
        return $this->data[$this->field->column_name] = $this->field->php_to_db($value);
    }
}

class CharField extends Field
{
    static $name = "string";

    function __construct($options)
    {
        parent::__construct($options);
        $this->limit = $this->get_option("limit");
    }

    protected function on_php_to_db($value)
    {
        try {
            $value = (string)$value;
        } catch ( Exception $e ) {
            throw new DbValidationError("Not a string");
        }
        if ( strlen($value) > $this->limit )
            throw new DbValidationError("Value too long");
        return $value;
    }

}
CharField::register();

class IntegerField extends Field
{
    static $name = "integer";

    function load_options()
    {
        parent::load_options();
        $this->auto_increment = $this->get_option("identity", false, false);
    }

    protected function on_php_to_db($value)
    {
        if ( is_numeric($value) )
            return (int)$value;
        throw new DbValidationError("Not an integer");
    }
}
IntegerField::register();

class BooleanField extends Field
{
    static $name = "boolean";

    protected function on_db_to_php($value)
    {
        return (bool)$value;
    }

    protected function on_php_to_db($value)
    {
        return (bool)$value;
    }
}
BooleanField::register();

class DateField extends Field
{
    static $name = "date";
    static $format = "Y-m-d";

    protected function on_db_to_php($value)
    {
        if ( !$value )
            return null;
        return DateTime::createFromFormat(static::$format, $value);
    }

    protected function on_php_to_db($value)
    {
        if ( is_string($value) )
        {
            try {
                $value = new DateTime($value);
            } catch(Exception $e) {
            }
        }

        if ( $value instanceof DateTime )
            return $value->format(static::$format);

        throw new DbValidationError("Not a valid " . static::$name);
    }

}
DateField::register();

class DateTimeField extends DateField
{
    static $name = "datetime";
    static $format = "Y-m-d H:i:s";
}
DateTimeField::register();

class DecimalField extends Field
{
    static $name = "decimal";

    protected function on_php_to_db($value)
    {
        if ( is_numeric($value) )
            return $value;
        throw new DbValidationError("Not a number");
    }

    protected function on_db_to_php($value)
    {
        return (float)$value;
    }
}
DecimalField::register();
