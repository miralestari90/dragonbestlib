#!/usr/bin/env php
<?php
require_once(__dir__."/../../../vendor/autoload.php");
require_once(__dir__."/../migration.php");
require_once(__dir__."/../../util.php");

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Phinx\Console\Command\AbstractCommand;
use Phinx\Console\Command\Create;
use Phinx\Console\Command\Migrate;
use Phinx\Util\Util;
use Phinx\Migration\AbstractTemplateCreation;


function get_migration_structure(
    AbstractCommand $command, InputInterface $input, OutputInterface $output)
{
    require_once($input->getArgument("model_file"));
    $env = $command->getConfig()->getDefaultEnvironment();

    $migrations = $command->getManager()->getMigrations($env);
    Model::load_models();
    $structure = new MigrationStructure();
    foreach ( $migrations as $migration )
    {
        if ( is_subclass_of($migration, "BaseMigration") )
        {
            $migration->set_structure_only($structure);
            $migration->change_models();
        }
    }
    return $structure;
}


class DurgMigrationCheck extends AbstractCommand
{

    protected function configure()
    {
        parent::configure();
        $this->addArgument("model_file", InputArgument::REQUIRED, "Path to the model file");
        $this->setName("check")
            ->setDescription("Check if models match migrations")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->bootstrap($input, $output);
        $delta = get_migration_structure($this, $input, $output)->delta();
        if ( $delta->has_changes() )
        {
            $output->writeln("Found following changes:");
            foreach ( $delta->children as $model => $model_delta )
            {
                if ( $model_delta->has_changes() )
                {
                    $output->writeln("  $model");
                    if ( $model_delta->status & MigrationStructureDelta::CREATED )
                        $output->writeln("    (new model)");
                    if ( $model_delta->status & MigrationStructureDelta::DELETED )
                        $output->writeln("    (deleted model)");
                    if ( $model_delta->status & MigrationStructureDelta::MODIFIED )
                        $output->writeln("    (modified options)");
                    if ( $model_delta->status & MigrationStructureDelta::CHILDREN )
                        $this->print_delta_fields($model_delta, $output);
                }
            }
        }
        else
        {
            $output->writeln("No change");
        }
    }

    private function print_delta_fields(
        MigrationStructureDelta $model_delta,
        OutputInterface $output
    )
    {
        foreach ( $model_delta->children as $field => $field_delta )
        {
            if ( $field_delta->has_changes() )
            {
                if ( $field_delta->status & MigrationStructureDelta::CREATED )
                    $output->write("    create");
                elseif ( $field_delta->status & MigrationStructureDelta::DELETED )
                    $output->write("    delete");
                elseif ( $field_delta->status & MigrationStructureDelta::MODIFIED )
                    $output->write("    modify");
                else
                    continue;
                $output->writeln(" $field");

            }
        }
    }
}

class TemplateCreation extends AbstractTemplateCreation
{
    function getMigrationTemplate()
    {
        return file_get_contents(dirname(__dir__)."/templates/migration_template.php");
    }

    function postMigrationCreation($filePath, $className, $baseClassName)
    {
        $contents = file_get_contents($filePath);
        $command = $this->input->command;
        $delta = get_migration_structure($command, $this->input, $this->output)->delta();
        $data = [
            '$generatedMigration' => $delta->to_migration(),
            '$includePath' => path_relative($this->include_file_path(), dirname($filePath)),
            'extends AbstractMigration' => "extends BaseMigration",
            '//REPLACE' => '',
        ];
        $contents = strtr($contents, $data);

        if (file_put_contents($filePath, $contents) === false) {
            throw new \RuntimeException(sprintf(
                'The file "%s" could not be written to',
                $filePath
            ));
        }
    }

    function include_file_path()
    {
        return __dir__ . "/../migration.php";
    }
}

class DurgMigrationCreate extends Create
{
    protected function configure()
    {
        $this->addArgument("model_file", InputArgument::REQUIRED, "Path to the model file");
        parent::configure();
        // Hacks to add default values to some arguments / options
        $name_arg = $this->getDefinition()->getArgument("name");
        $rc = new ReflectionClass($name_arg);
        $mode = $rc->getProperty("mode");
        $mode->setAccessible(true);
        $mode->setValue($name_arg, InputArgument::OPTIONAL);
        $name_arg->setDefault("AutoMigration".Util::getCurrentTimestamp());

        $this->getDefinition()->getOption("class")->setDefault("TemplateCreation");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $input->command = $this;
        require_once($input->getArgument("model_file"));

        $this->bootstrap($input, $output);
        $paths = $this->getConfig()->getMigrationPaths();
        if ( sizeof($paths) == 1 && !file_exists($paths[0]) )
        {
            mkdir($paths[0], 0775, true);
        }

        parent::execute($input, $output);
    }

}

$application = new Application("migration", "0.0.1");
$application->addCommands([
    new DurgMigrationCheck(),
    new DurgMigrationCreate(),
    new Migrate(),

]);

$application->run();
