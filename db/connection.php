<?php

require_once("errors.php");


class Connection
{
    private static $instance = null;

    private function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    static function instance() : Connection
    {
        if ( static::$instance === null )
            throw new DbConfigurationError("Database not connected");
        return static::$instance;
    }

    static function is_connected()
    {
        return static::$instance !== null;
    }

    static function connect($dsn, $username=null, $password=null, $options=[]) : Connection
    {
        $class = static::class;
        static::$instance = new $class(new PDO($dsn, $username, $password, $options));
        return static::$instance;
    }

    function execute($query, $variables = [], $throw_on_error=false)
    {
        $statement = $this->pdo->prepare($query);

        if ( $statement )
        {

            foreach ( $variables as $name => $value )
            {
                if ( is_int($value) )
                    $type = pdo::PARAM_INT;
                else if ( is_bool($value) )
                    $type = pdo::PARAM_BOOL;
                else if ( $value === null )
                    $type = pdo::PARAM_NULL;
                else
                    $type = pdo::PARAM_STR;
                $statement->bindValue($name, $value, $type);
            }

            if ( $statement->execute() )
            {
                return $statement;
            }
        }

        $error = implode(": ", $this->pdo->errorInfo());
        $msg = "Query failed: $error: $query\n".print_r($variables, true);
        if ( $throw_on_error )
            throw new DbRuntimeError($msg);
        else
            user_error($msg, E_USER_WARNING);
        return null;
    }
}
