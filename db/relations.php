<?php

require_once("fields.php");


class ForeignKeyField extends Field
{
    static $name = "foreign_key";

    protected function load_options()
    {
        /// Class this relation pointing to
        $this->target_model = $this->get_option(1);

        // NOTE: This calls the target model init_fields(),
        // which means that tables forming circular dependencies of
        // foreign keys might not work
        $this->target_model::init_fields();

        /// Field name this relation is pointing to
        $this->target_field = $this->get_option("to_field", false, $this->target_model::$primary_key);
        /// Field object this of target_model / target_field
        $this->related_field = $this->target_model::get_unbound_field($this->target_field);
        parent::load_options();
        /// Name of the reverse relation
        $this->related_name = $this->get_option("related_name", false,
            Model::to_table_name($this->model_name)."s"
        );

        /// Reverse relation field
        $this->reverse_relation = new ReverseForeignKeyField($this);
        $this->target_model::add_dynamic_field($this->reverse_relation);

    }

    protected function on_php_to_db($value)
    {
        if ( $value instanceof $this->target_model )
            $value = $value->pk();
        return $this->related_field->php_to_db($value);
    }

    protected function on_db_to_php($value)
    {
        return $this->target_model::fetch($value, $this->target_field);
    }

    function default_column_name()
    {
        return "{$this->field_name}_{$this->target_field}";
    }

}
ForeignKeyField::register();

class ReverseForeignKeyField extends Field
{
    function __construct(ForeignKeyField $forward_field)
    {
        /// Class this relation pointing to
        $this->target_model = $forward_field->model_name;
        /// Field name this relation is pointing to
        $this->target_field = $forward_field->field_name;
        /// Field object this of target_model / target_field
        $this->related_field = $forward_field;
        /// Name of the reverse relation
        $this->related_name = $forward_field->field_name;
        /// Reverse relation field
        $this->reverse_relation = $forward_field;

        /// Actual field in this model the relation is pointing to
        $this->underlying_field = $this->related_field;

        parent::__construct([
            "column_name" => $forward_field->related_field->column_name,
            "field_name" => $forward_field->related_name,
            "model_name" => $forward_field->target_model,
            "default" => [],
        ]);
    }

    function db_to_php($value)
    {
        if ( $value === null )
            return [];
        $value = $this->underlying_field->db_to_php($value);
        return $this->target_model::query()->where($this->related_field->column_name, '=', $value);
    }
}

class ManyToManyField extends Field
{
    static $name = "manytomany";

    function load_options()
    {
        parent::load_options();
        $this->options["has_column"] = false;

        /// Class this relation pointing to
        $this->target_model = $this->get_option(1);

        /// Field name this relation is pointing to
        $this->target_field = $this->get_option("to_field", false, $this->target_model::$primary_key);

        /// Field object this of target_model / target_field
        $this->related_field = $this->target_model::get_unbound_field($this->target_field);

        /// Name of the reverse relation
        $this->related_name = $this->get_option("related_name", false,
            Model::to_table_name($this->model_name)."s"
        );


        /// Field name this relation is being pointed to
        $this->source_field_name = $this->get_option("source_field", false, $this->model_name::$primary_key);
        $this->column_name = $this->source_field_name;
        $this->underlying_field = null; /// Cannot get at this stage


        /// Table this relations goes through
        $this->through = $this->get_option("through", false,
            Model::to_table_name("{$this->model_name}_{$this->target_model}")
        );
        /// Column name in \b through which points to the related table
        $this->through_target_column = $this->get_option(
            "through_target_column", false,
            Model::to_table_name($this->target_model)."_{$this->target_field}"
        );
        /// Column name in \b through which points to the source table
        $this->through_source_column = $this->get_option(
            "through_source_column", false,
            Model::to_table_name($this->model_name)."_{$this->source_field_name}"
        );

        /// Reverse relation field
        $this->is_reverse = false;
        $this->reverse_relation = $this->make_reverse();
        $this->target_model::add_dynamic_field($this->reverse_relation);
    }

    protected function make_reverse()
    {
        $refl = new ReflectionClass(static::class);
        $reverse = $refl->newInstanceWithoutConstructor();
        $reverse->options = [];
        $reverse->field_name = $this->related_name;
        $reverse->model_name = $this->target_model;
        $reverse->null = $this->null;
        $reverse->default = $this->default;

        $reverse->target_model = $this->model_name;
        $reverse->target_field = $this->source_field_name;
        $reverse->related_field = $this->underlying_field;
        $reverse->related_name = $this->field_name;
        $reverse->underlying_field = $this->related_field;
        $reverse->reverse_relation = $this;
        $reverse->is_reverse = true;

        $reverse->source_field_name = $this->target_field;
        $reverse->column_name = $this->related_field->column_name;
        $reverse->through = $this->through;
        $reverse->through_target_column = $this->through_source_column;
        $reverse->through_source_column = $this->through_target_column;

        return $reverse;
    }

    function __construct($options)
    {
        $options["column_name"] = "";
        $options["default"] = [];
        parent::__construct($options);
    }

    function db_to_php($value)
    {
        if ( $value === null )
            return $this->default;

        if ( $this->underlying_field === null )
        {
            $this->underlying_field = $this->model_name::get_unbound_field($this->source_field_name);
        }

        if ( $this->related_field === null )
        {
            $this->related_field = $this->target_model::get_unbound_field($this->target_field);
        }

        $value = $this->underlying_field->db_to_php($value);
        return $this->target_model::query()
            ->join_table(
                $this->through,
                $this->related_field->column_name,
                new QueryColumn("{$this->target_model}.{$this->related_field->column_name}")
            )
            ->where("{$this->through}.{$this->target_field}", '=', $value);
    }

    function to_migration_options()
    {
        if ( $this->is_reverse )
            return null;
        return parent::to_migration_options();
    }
}
ManyToManyField::register();
