<?php

/**
 * \brief Concatenates two path portions, avoiding duplicate slashes
 */
function path_join($p1, $p2)
{
    return rtrim($p1, "/") . "/" . ltrim($p2, "/");
}

/**
 * \brief Remove redundant parts of a path
 */
function path_canonicalize($path)
{
    $output = [];
    $components = explode("/", $path);
    foreach ( $components as $component )
    {
        if ( $component == ".." )
            array_pop($output);
        elseif ( $component != "" && $component != "." )
            array_push($output, $component);
    }

    $result = implode("/", $output);

    if ( $path )
    {
        if ( $path[strlen($path)-1] == "/" )
            $result .= "/";
        if ( $path[0] == "/" && strlen($result) > 1 )
            $result = "/" . $result;
    }

    return $result;
}

/**
 * \brief Returns a path pointing to \p target which is relative to \p relative_to
 */
function path_relative($target, $relative_to)
{
    # /foo/bar/baz  /foo/bar        => ./baz
    # /foo/bar      /foo/bar/baz    => ..
    # /foo/bar/b1   /foo/bar/b2     => ../b1
    # /foo/bar      /bar/foo        => ../../foo/bar
    # /foo/bar      /foo/bar        => .
    $separator = "/";
    $target = explode($separator, path_canonicalize($target));
    $relative_to = explode($separator, path_canonicalize($relative_to));
    while ( $target && $relative_to && $target[0] == $relative_to[0] )
    {
        array_shift($target);
        array_shift($relative_to);
    }

    if ( !$target && !$relative_to )
        return ".";

    return str_repeat("../", sizeof($relative_to)) . implode("/", $target);
}

function slug_to_title($slug)
{
    return ucwords(str_replace(["_", "-"], " ", $slug));
}

function title_to_slug($title)
{
    return strtolower(preg_replace("([^-a-zA-Z0-9_])", "", str_replace(" ", "-", $title)));
}


function string_array_to_string($array, $indent=false, $padding="")
{
    if ( $indent )
    {
        $sep = "\n$padding    ";
        $arrow = " => ";
    }
    else
    {
        $sep = "";
        $arrow = "=>";
    }
    $numid = 0;
    $out = "{$padding}[";
    foreach ( $array as $key => $value )
    {
        if ( is_array($value) )
            $value = string_array_to_string($value);
        else if ( $value === true )
            $value = 'true';
        else if ( $value === false )
            $value = 'false';
        else if ( $value === null )
            $value = 'null';
        else if ( !is_int($value) && !is_float($value) )
            $value = "'$value'";

        if ( $key === $numid )
            $out .= "$sep$value,";
        else
            $out .= "$sep'$key'$arrow$value,";
        $numid++;
    }
    if ( $indent )
        $out .= "\n$padding";
    $out .= "]";
    return $out;
}
