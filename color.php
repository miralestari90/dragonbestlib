<?php

/**
 * \brief Color description interface
 */
abstract class Color
{

    function __toString()
    {
        return $this->to_html();
    }

    /**
     * \brief Multiply by a [0,1] value
     */
    abstract function multiply($value);

    /**
     * \brief Add a [0,1] value
     */
    abstract function add($value);

    /**
     * \brief Returns an html color string
     */
    abstract function to_html();

    /**
     * \brief Returns a css color string
     */
    function to_css()
    {
        return $this->to_html();
    }

    /**
     * \brief Returns a rgb color
     */
    abstract function to_rgb();

    /**
     * \brief Luma component
     */
    abstract function luma();


    /**
     * \brief Parses a CSS color description
     */
    static function from_css($css_string)
    {
        $css_regex = "(".
            "(#(?P<hex6>(?P<hr>[0-9a-f]{2})(?P<hg>[0-9a-f]{2})(?P<hb>[0-9a-f]{2})))|".
            "(#(?P<hex3>[0-9a-f]{3}))|".
            "(rgba?\(\s*(?P<rgb>(?P<r>[0-9]+)\s*,\s*(?P<g>[0-9]+)\s*,\s*(?P<b>[0-9]+)\s*(,\s*[0-9]+\s*)?)\))".
            ")";
        $matches = [];
        if ( !preg_match($css_regex, strtolower($css_string), $matches) )
            return new Nocolor();

        if ( $matches["hex6"] )
            return new ColorRGB(hexdec($matches["hr"]), hexdec($matches["hg"]), hexdec($matches["hb"]));

        if ( $matches["hex3"] )
        {
            $r = $matches["hex3"][0];
            $g = $matches["hex3"][1];
            $b = $matches["hex3"][2];
            return new ColorRGB(hexdec($r.$r), hexdec($g.$g), hexdec($b.$b));
        }

        return new ColorRGB((int)$matches["r"], (int)$matches["g"], (int)$matches["b"]);
    }

    /**
     * \brief Allocate as a GD color
     */
    abstract function allocate_gd($image);
}

class Nocolor extends Color
{
    function luma() { return 0; }
    function multiply($value) {}
    function add($value) {}
    function to_html() { return ""; }
    function to_css() { return "transparent"; }
    function to_rgb() { return new ColorRGB(); }
    function allocate_gd($image) { return imagecolortransparent($image); }
}

class ColorRGB extends Color
{
    public $r, $g, $b;

    function __construct ($r=0, $g=0, $b=0)
    {
        $this->r = $r;
        $this->g = $g;
        $this->b = $b;
    }

    function luma()
    {
        return (0.3*$this->r + 0.59*$this->g + 0.11*$this->b) / 255;
    }

    function multiply($value)
    {
        $this->r = (int)($this->r*$value);
        $this->g = (int)($this->g*$value);
        $this->b = (int)($this->b*$value);
    }

    function add($value)
    {
        $this->r = (int)min($this->r+$value*255, 255);
        $this->g = (int)min($this->g+$value*255, 255);
        $this->b = (int)min($this->b+$value*255, 255);
    }

    function to_html()
    {
        return sprintf("#%02x%02x%02x", $this->r, $this->g, $this->b);
    }

    function to_rgb()
    {
        return $this;
    }

    function to_css()
    {
        return sprintf("rgb(%d, %d, %d)", $this->r, $this->g, $this->b);
    }

    function allocate_gd($image)
    {
        return imagecolorallocate($image, $this->r, $this->g, $this->b);
    }
}

/**
 * \brief Color palette
 *
 * Colors can be accessed using subscript (read/write) or as properties (read only)
 */
class Palette implements ArrayAccess
{
    /**
     * \brief Loads a GIMP Palette file
     * \returns a Palette object populated with values from the file
     */
    static function from_file($filename)
    {
        $lines = file($filename);
        if ( empty($lines) || trim($lines[0]) != "GIMP Palette" )
        {
            trigger_error("$filename is not a valid palette file", E_USER_WARNING);
            return new Palette();
        }

        array_shift($lines);

        $reading_headers = true;
        $palette = new Palette();

        foreach ( $lines as $line )
        {
            $line = trim($line);
            // Skip comments / blank lines
            if ( !$line || $line[0] == '#')
                continue;

            $matches = array();
            if ( $reading_headers )
            {
                if ( preg_match("/(\w)+:\s*(.+)/", $line, $matches) )
                {
                    $lower_key = strtolower($matches[1]);
                    if ( $lower_key == "name" )
                        $palette->name = $matches[2];
                    elseif ( strtolower($lower_key) == "columns" )
                        $palette->columns = $matches[2];
                    else
                        $palette->metadata[$matches[1]] = $matches[2];
                    continue;
                }
                $reading_headers = false;
            }

            if ( !preg_match("/(?P<red>\d+)\s+(?P<green>\d+)\s+(?P<blue>\d+)\s*(?P<name>.*)/", $line, $matches) )
            {
                trigger_error("$filename contains invalid color definitions", E_USER_WARNING);
                continue;
            }

            $color = new ColorRGB((int)$matches["red"], (int)$matches["green"], (int)$matches["blue"]);
            $color_name = strtolower(str_replace(" ", "_", $matches["name"]));
            if ( !$color_name )
            {
                $palette []= $color;
            }
            else
            {
                $palette[$color_name] = $color;
            }
        }

        return $palette;
    }

    function __construct($colors = array(), $name = "", $columns = 0, $metadata = array())
    {
        $this->colors = $colors;
        $this->name = $name;
        $this->columns = $columns;
        $this->metadata = $metadata;
    }

    function __get($property)
    {
        if (property_exists($this, $property))
            return $this->$property;
        return $this->colors[$property];
    }

    public function offsetExists($offset)
    {
        return isset($this->colors[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->colors[$offset];
    }

    public function offsetSet($offset, $value)
    {
        return $this->colors[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->colors[$offset]);
    }
}
