<?php

/**
 * \brief Page mixin to help set up a not-found page
 *
 * Sets the render argument "path" to the path that hasn't been found
 */
trait NotFoundTrait
{
    function dispatch($path)
    {
        http_response_code(404);
        $this->render(array("path" => $path));
        return true;
    }
}


/**
 * \brief Page mixin to handle errors.
 *
 * Always renders the current path, passing it to as a render argument
 * \note Unlike NotFoundTrait this does not set an error code
 */
trait ErrorTrait
{
    function dispatch($path)
    {
        $this->render(array("path" => $path));
        return true;
    }
}
