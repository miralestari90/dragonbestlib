<?php

trait SimpleSvgContainer
{
    function containing_element()
    {
        return $this;
    }

    function polygon($points, $style=null)
    {
        $path = "";
        if ( sizeof($points) > 0 )
        {
            list($x, $y) = array_shift($points);
            $path = "M $x $y";
            foreach ( $points as $point )
            {
                list($x, $y) = $point;
                $path .= " L $x $y";
            }
            $path .= "Z";
        }
        return new SimpleSvg($this->containing_element(), "path", ["d" => $path], $style);
    }

    function circle($cx, $cy, $radius, $style=null)
    {
        return new SimpleSvg(
            $this->containing_element(),
            "circle",
            ["cx"=> $cx, "cy" => $cy,  "r" => $radius],
            $style
        );
    }

    function path($style=null)
    {
        return new SimpleSvgPath($this->containing_element(), "path", [], $style);
    }

    function rect($x, $y, $width, $height, $style=null)
    {
        return new SimpleSvg(
            $this->containing_element(),
            "rect",
            ["x"=>$x, "y"=>$y, "width"=>$width, "height"=>$height],
            $style
        );
    }
}


class SimpleSvgImage extends DomDocument
{
    use SimpleSvgContainer;

    function __construct($version="1.0", $encoding="UTF-8")
    {
        parent::__construct($version, $encoding);
        $this->registerNodeClass("DomElement", "SimpleSvgElement");
    }

    static function new_image($width, $height)
    {
        $document = new SimpleSvgImage();
        new SimpleSvg($document, "svg", ["width"=>$width, "height"=>$height]);
        return $document;
    }

    function load($filename, $options=null)
    {
        parent::load($filename, $options);

        foreach ( $this->getElementsByTagName('*') as $element )
        {
            if ( $element->hasAttribute("id") )
                $element->setIdAttribute("id", true);
        }
    }

    function __tostring()
    {
        return $this->saveXML($this);
    }

    function to_imagick()
    {
        $im = new Imagick();
        $im->setResolution(96, 96);
        $im->setBackgroundColor("transparent");
        $im->readImageBlob((string)$this);
        $im->setImageType(Imagick::IMGTYPE_TRUECOLORMATTE);
        $im->setImageColorSpace(Imagick::COLORSPACE_SRGB);
        $im->profileImage("*", null);
        $im->setImageGamma(0);
        return $im;
    }

    function containing_element()
    {
        return $this->documentElement;
    }
}

class SimpleSvgElement extends DomElement
{
    function __construct($name, $value=null, $namespaceURI="http://www.w3.org/2000/svg")
    {
        parent::__construct($name, $value, $namespaceURI);
    }

    function style()
    {
        $chunks = explode(";", $this->getAttribute("style"));
        $array = [];
        foreach ( $chunks as $chunk )
        {
            if ( !$chunk )
                continue;
            list($name, $value) = explode(":", $chunk);
            $array[trim($name)] = trim($value);
        }
        return $array;
    }

    function style_feature($name, $default=null)
    {
        $style = $this->style();
        return $style[$name] ?? $default;
    }

    function set_style_feature($name, $value)
    {
        $style = $this->style();
        $style[$name] = $value;
        $this->set_style($style);
    }

    function set_style($style)
    {
        $style_str = "";
        foreach ( $style as $name => $value )
            $style_str .= "$name:$value;";
        $this->setAttribute("style", $style_str);
    }
}

class SimpleSvg extends SimpleSvgElement
{
    function __construct($parent, $tag, $attrs=[], $style=null)
    {
        parent::__construct($tag);
        $parent->appendChild($this);

        if ( $style !== null )
        {
            $attrs["style"] = $this->style_to_css($style);
        }
        foreach ( $attrs as $name => $value )
            $this->setAttribute($name, $value);
    }

    function style_to_css($style)
    {
        if ( is_string($style) )
            return $style;
        if ( $style === null )
            return "";

        $result = "";
        foreach ( $style as $name => $value )
            $result .= "$name: $value;";
        return $result;
    }

    function set_style($style)
    {
        $this->setAttribute("style", $this->style_to_css($style));
        return $this;
    }
}

class SimpleSvgPath extends SimpleSvg
{
    function __construct($parent, $tag, $attrs=[], $style=null)
    {
        parent::__construct($parent, $tag, $attrs, $style);

        if ( $this->hasAttribute("d") )
        {
            $this->d = $this->getAttributeNode("d");
            $this->d->value .= " ";
        }
        else
        {
            $this->d = $this->setAttribute("d", "");
        }
    }

    function d($char, $args)
    {
        $s_args = implode(" ", $args);
        $this->d->value .= "$char $s_args ";
        return $this;
    }

    private function rel_d($char, $args, $relative)
    {
        return $this->d($this->relativize($char, $relative), $args);
    }

    private function relativize($char, $relative)
    {
        if ( $relative )
            return strtolower($char);
        return $char;
    }

    function move($x, $y, $relative=false)
    {
        return $this->rel_d("M", [$x, $y], $relative);
    }

    function line($x, $y, $relative=false)
    {
        return $this->rel_d("L", [$x, $y], $relative);
    }

    function close()
    {
        return $this->d("Z", [$x, $y]);
    }

    function horizontal($x, $relative=false)
    {
        return $this->rel_d("H", [$x], $relative);
    }

    function vertical($y, $relative=false)
    {
        return $this->rel_d("V", [$y], $relative);
    }

    function curve($x1, $y1, $x2, $y2, $x, $y, $relative=false)
    {
        return $this->rel_d("C", [$x1, $y1, $x2, $y2, $x, $y], $relative);
    }

    function smooth($x2, $y2, $x, $y, $relative=false)
    {
        return $this->rel_d("S", [$x2, $y2, $x, $y], $relative);
    }

    function quadratic($x1, $y1, $x, $y, $relative=false)
    {
        return $this->rel_d("Q", [$x1, $y1,$x, $y], $relative);
    }

    function smooth_quadratic($x, $y, $relative=false)
    {
        return $this->rel_d("T", [$x, $y], $relative);
    }

    function arc($rx, $ry, $x_axis_rotation, $large_arc_flag, $sweep_flag, $x, $y, $relative=false)
    {
        return $this->rel_d("A", [$rx, $ry, $x_axis_rotation, $large_arc_flag, $sweep_flag, $x, $y], $relative);
    }

}


